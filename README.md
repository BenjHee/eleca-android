# Eleca - Android

Eleca is electronic developpement companion app. By defining a system with resistor capacitor or inductor, Eleca compute thebest components for any given system defined with these component.

Eleca works by testing every possible combination of component for a given system, and listing the best results.

## What's new

*new feature will be added in this section*

## Features

- Voltage divider

- User defined systems

## Screenshots

<img src="assets/screenshots/Screenshot_01.png" width=50%>
<img src="assets/screenshots/Screenshot_02.png" width=50%><img src="assets/screenshots/Screenshot_03.png" width=50%>

## TODO

- Operational amplifier

- Filters

- Translations

## Download

*The app is not currenly available as a stable realease, however the app can be buid using the source*

## Dependencies and library

- [exp4j](https://www.objecthunter.net/exp4j/) : exp4j is capable of evaluating expressions and functions in the real domain

## FAQ
