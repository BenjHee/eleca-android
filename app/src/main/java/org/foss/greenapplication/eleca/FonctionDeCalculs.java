package org.foss.greenapplication.eleca;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by infinite on 15/09/17.
 */

public class FonctionDeCalculs
{
    public double getR1(){
        return valeurR1;
    }

    public double getSolutionR1(){
        return solutionR1;
    }

    public double getSolutionR1Puissance(){
        return puissanceR1;

    }

    public double getR2 (){
        return valeurR2;
    }

    public double getSolutionR2() {
        return solutionR2;
    }

    public double getSolutionR2Puissance(){
        return puissanceR2;
    }

    public double getSolutionRapport() {
        return solutionRapport;
    }

    public double getEcartPuissance() {
        return ecartPuissance;
    }

    private double valeurR1;
    private double valeurR2;
    private double puissanceR1;
    private double puissanceR2;
    private double solutionR1;
    private double solutionR2;
    private double solutionRapport;
    private double ecartPuissance;


    /**
     * Calcule le rapport du pont diviseur avec en parametre le ratio, ou les entree et sortie
     * @param rapportCible valeur du ration a atteindre
     * @param selectionE3 série E3 a ajouté a la liste de valeur a calculer
     * @param selectionE6 série E6 a ajouté a la liste de valeur a calculer
     * @param selectionE12 série E12 a ajouté a la liste de valeur a calculer
     * @param selectionE24 série E24 a ajouté a la liste de valeur a calculer
     * @param selectionE48 série E48 a ajouté a la liste de valeur a calculer
     * @param selectionE96 série E96 a ajouté a la liste de valeur a calculer
     */
    public void calculPontDiviseur (float rapportCible, boolean selectionE3, boolean selectionE6, boolean selectionE12, boolean selectionE24, boolean selectionE48, boolean selectionE96)
    {

        int resistanceSerieE3[]  = {100,220,470};
        int resistanceSerieE6[]  = {100,150,220,330,470,680};
        int resistanceSerieE12[] = {100,120,150,180,220,270,330,390,470,560,680,820};
        int resistanceSerieE24[] = {100,110,120,130,150,160,180,200,220,240,270,300,330,360,390,430,470,510,560,620,680,750,820,910};
        int resistanceSerieE48[] = {100,105,110,115,121,127,133,140,147,154,162,169,178,187,196,205,215,226,237,249,261,274,287,301,316,332,348,365,383,402,422,442,464,487,511,536,562,590,619,649,681,715,750,787,825,866,909,953};
        int resistanceSerieE96[] = {100,102,105,107,110,113,115,118,121,124,127,130,133,137,140,143,147,150,154,158,162,165,169,174,178,182,187,191,196,200,205,210,215,221,226,232,237,243,249,255,261,267,274,280,287,294,301,309,316,324,332,340,348,357,365,374,383,392,402,412,422,432,442,453,464,475,487,499,511,523,536,549,562,576,590,604,619,634,649,665,681,698,715,732,750,768,787,806,825,845,866,887,909,931,953,976};

        Set<Integer> resistanceDisponible = new HashSet<>(3);
        List<Integer> selectionSerie = new ArrayList<>();

        if (selectionE3 == true)
        {
            selectionSerie.add(3);
        }
        if (selectionE6 == true)
        {
            selectionSerie.add(6);
        }
        if (selectionE12 == true)
        {
            selectionSerie.add(12);
        }
        if (selectionE24 == true)
        {
            selectionSerie.add(24);
        }
        if (selectionE48 == true)
        {
            selectionSerie.add(48);
        }
        if (selectionE96  == true)
        {
            selectionSerie.add(96);
        }

        int selectionSerieSize = selectionSerie.size();
        // Creation de la liste de resistance à calculer

        for (int i = 0; i < selectionSerieSize; i++)
        {
            switch (selectionSerie.get(i))
            {
                case 3:
                    for (int arrayIndex = 0; arrayIndex < 3; arrayIndex++)
                    {
                        resistanceDisponible.add(resistanceSerieE3[arrayIndex]);
                    }
                    break;
                case 6:
                    for (int arrayIndex = 0; arrayIndex < 6; arrayIndex++)
                    {
                        resistanceDisponible.add(resistanceSerieE6[arrayIndex]);
                    }
                    break;
                case 12:
                    for (int arrayIndex = 0; arrayIndex < 12; arrayIndex++)
                    {
                        resistanceDisponible.add(resistanceSerieE12[arrayIndex]);
                    }
                    break;
                case 24:
                    for (int arrayIndex = 0; arrayIndex < 24; arrayIndex++)
                    {
                        resistanceDisponible.add(resistanceSerieE24[arrayIndex]);
                    }
                    break;
                case 48:
                    for (int arrayIndex = 0; arrayIndex < 48; arrayIndex++)
                    {
                        resistanceDisponible.add(resistanceSerieE48[arrayIndex]);
                    }
                    break;
                case 96:
                    for (int arrayIndex = 0; arrayIndex < 96; arrayIndex++)
                    {
                        resistanceDisponible.add(resistanceSerieE96[arrayIndex]);
                    }
                    break;
            }

        }

        double rapport = -1;
        double differenceRapport = Double.POSITIVE_INFINITY;
        double score = Double.POSITIVE_INFINITY;

        for (int r1 : resistanceDisponible)
        {
            for(int r2: resistanceDisponible) {
                for (double r1Puissance = 0.1; r1Puissance <= 100000000;r1Puissance = r1Puissance * 10)
                {
                    for (double r2Puissance = 0.1; r2Puissance <= 100000000;r2Puissance = r2Puissance * 10)
                    {
                        rapport = (r2*r2Puissance)/((r1*r1Puissance)+(r2*r2Puissance));
                        differenceRapport = Math.abs(rapport - rapportCible);//DifferenceAbsolue(rapport, rapportCible);
                        if(differenceRapport < 0.000000001)
                        {
                            valeurR1 = r1;
                            valeurR2 = r2;
                            solutionR1 = r1 * r1Puissance;
                            solutionR2 = r2 * r2Puissance;
                            puissanceR1 = Math.log10(r1Puissance);
                            puissanceR2 = Math.log10(r2Puissance);
                            solutionRapport = (r2*r2Puissance)/((r1*r1Puissance)+(r2*r2Puissance));
                            ecartPuissance = puissanceR1 - puissanceR2;
                        }
                        else
                        {
                            if (differenceRapport < score)
                            {
                                score = differenceRapport;
                                valeurR1 = r1;
                                valeurR2 = r2;
                                solutionR1 = r1 * r1Puissance;
                                solutionR2 = r2 * r2Puissance;
                                puissanceR1 = Math.log10(r1Puissance);
                                puissanceR2 = Math.log10(r2Puissance);
                                ecartPuissance = puissanceR1 - puissanceR2;
                                solutionRapport = (r2*r2Puissance)/((r1*r1Puissance)+(r2*r2Puissance));
                            }
                        }
                    }
                }
            }
        }
    }

}
