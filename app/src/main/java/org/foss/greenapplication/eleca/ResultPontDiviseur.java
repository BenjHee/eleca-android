package org.foss.greenapplication.eleca;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;


import static android.R.attr.defaultValue;

public class ResultPontDiviseur extends AppCompatActivity
{
    private java.lang.String formatResistance(double valeurResistance)
    {
        String stringValeurResistance = "";
        if(valeurResistance >= 1_000_000_000)
        {
            valeurResistance =valeurResistance /1_000_000_000;
            return (stringValeurResistance.format("%.1f",valeurResistance) + getString(R.string.text_common_E9ohm));
        }
        else if (valeurResistance >= 1_000_000)
        {
            valeurResistance =valeurResistance /1_000_000;
            return (stringValeurResistance.format("%.1f",valeurResistance)  + getString(R.string.text_common_E6ohm));
        }
        else if (valeurResistance >= 1_000)
        {
            valeurResistance =valeurResistance /1_000;
            return (stringValeurResistance.format("%.1f",valeurResistance) + getString(R.string.text_common_E3ohm));
        }
        else
        {
            return (stringValeurResistance.format("%.1f",valeurResistance) + " " + getString(R.string.text_common_E0ohm));
        }
    }
    private void setImageViewBackground(ImageView image,int bande)
    {
        switch (bande)
        {
            case 0: image.setBackgroundResource(R.color.ColorResistance0);break;
            case 1: image.setBackgroundResource(R.color.ColorResistance1);break;
            case 2: image.setBackgroundResource(R.color.ColorResistance2);break;
            case 3: image.setBackgroundResource(R.color.ColorResistance3);break;
            case 4: image.setBackgroundResource(R.color.ColorResistance4);break;
            case 5: image.setBackgroundResource(R.color.ColorResistance5);break;
            case 6: image.setBackgroundResource(R.color.ColorResistance6);break;
            case 7: image.setBackgroundResource(R.color.ColorResistance7);break;
            case 8: image.setBackgroundResource(R.color.ColorResistance8);break;
            case 9: image.setBackgroundResource(R.color.ColorResistance9);break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result_pont_diviseur);

        Intent receiveIntent = this.getIntent();
        float ratioIn = getIntent().getFloatExtra("pontdiviseur.rapportCiblePontDiviseur",defaultValue);
        double Vs = getIntent().getDoubleExtra("calculationSet.Vs",defaultValue);
        double ratioOut = getIntent().getDoubleExtra("pontdiviseur.rapportPontDiviseur", defaultValue);
        double resistanceR1 = getIntent().getDoubleExtra("pontdiviseur.solutionR1", defaultValue);
        double resistanceR2 = getIntent().getDoubleExtra("pontdiviseur.solutionR2", defaultValue);
        double valeurR1 = getIntent().getDoubleExtra("pontdiviseur.valeurResistanceR1", defaultValue);
        double valeurR2 = getIntent().getDoubleExtra("pontdiviseur.valeurResistanceR2", defaultValue);
        double puissanceR1 = getIntent().getDoubleExtra("pontdiviseur.puissanceR1", defaultValue);
        double puissanceR2 = getIntent().getDoubleExtra("pontdiviseur.puissanceR2", defaultValue);
        double ecartpuissance = getIntent().getDoubleExtra("pontdiviseur.ecartPuissance",defaultValue);

        int R1Val1 = (int) (valeurR1 /100);
        int R1Val2 = (int) ((valeurR1 % 100)-(valeurR1 % 10))/10;
        int R1Val3 = (int) (valeurR1 % 10);

        int R2Val1 = (int) (valeurR2 /100);
        int R2Val2 = (int) ((valeurR2 % 100)-(valeurR2 % 10))/10;
        int R2Val3 = (int) (valeurR2 % 10);
        int R1Val4,R2Val4;

        if (ecartpuissance < 0)
        {
            R1Val4 = 0;
            R2Val4 = (int) ecartpuissance * -1;
        }
        else
        {
            R1Val4 = (int) ecartpuissance;
            R2Val4 = 0;
        }

        ImageView Bande1Resistance1 = (ImageView) findViewById(R.id.imageViewBande1Resistance1);
        ImageView Bande2Resistance1 = (ImageView) findViewById(R.id.imageViewBande2Resistance1);
        ImageView Bande3Resistance1 = (ImageView) findViewById(R.id.imageViewBande3Resistance1);
        ImageView Bande4Resistance1 = (ImageView) findViewById(R.id.imageViewBande4Resistance1);

        ImageView Bande1Resistance2 = (ImageView) findViewById(R.id.imageViewBande1Resistance2);
        ImageView Bande2Resistance2 = (ImageView) findViewById(R.id.imageViewBande2Resistance2);
        ImageView Bande3Resistance2 = (ImageView) findViewById(R.id.imageViewBande3Resistance2);
        ImageView Bande4Resistance2 = (ImageView) findViewById(R.id.imageViewBande4Resistance2);

        setImageViewBackground(Bande1Resistance1,R1Val1);
        setImageViewBackground(Bande2Resistance1,R1Val2);
        setImageViewBackground(Bande3Resistance1,R1Val3);
        setImageViewBackground(Bande4Resistance1,R1Val4);

        setImageViewBackground(Bande1Resistance2,R2Val1);
        setImageViewBackground(Bande2Resistance2,R2Val2);
        setImageViewBackground(Bande3Resistance2,R2Val3);
        setImageViewBackground(Bande4Resistance2,R2Val4);


        TextView sortieCible = (TextView) findViewById(R.id.textViewSortieCibleValeur);
        TextView resultat = (TextView) findViewById(R.id.textViewResultatValeur);

        sortieCible.setText(""+(Math.round(ratioIn*1000000.0)/1000000.0));
        resultat.setText(""+(Math.round(ratioOut*1000000.0)/1000000.0));

        TextView textViewValeurR1 = (TextView) findViewById(R.id.textViewValDeR1);
        TextView textViewValeurR2 = (TextView) findViewById(R.id.textViewValDeR2);

        textViewValeurR1.setText(getResources().getString(R.string.text_activityPontDiviseurSimple_exempleR1)+" " +formatResistance(resistanceR1));
        textViewValeurR2.setText(getResources().getString(R.string.text_activityPontDiviseurSimple_exempleR2)+" " +formatResistance(resistanceR2));

        TextView ResultatVs = (TextView) findViewById(R.id.textViewResultatVs);
        TextView ValeurVs = (TextView) findViewById(R.id.textViewValeurVs);
        if (Vs == -1)
        {
            ResultatVs.setVisibility(View.INVISIBLE);
            ResultatVs.setClickable(false);

            ValeurVs.setVisibility(View.INVISIBLE);
            ValeurVs.setClickable(false);
        }
        else
        {
            ResultatVs.setVisibility(View.VISIBLE);
            ResultatVs.setClickable(true);

            ValeurVs.setVisibility(View.VISIBLE);
            ValeurVs.setClickable(true);
            ValeurVs.setText(String.format("%.3f",Vs) + " V");
        }
    }
}
