package org.foss.greenapplication.eleca;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;
import net.objecthunter.exp4j.tokenizer.UnknownFunctionOrVariableException;

import org.foss.greenapplication.eleca.UserFunctionEvaluationClasses.ObjectVariables;
import org.foss.greenapplication.eleca.UserFunctionEvaluationClasses.ObjectVariablesArray;


import java.util.ArrayList;
import java.util.HashSet;


public class UserFunction extends AppCompatActivity {

    UserFunctionEvaluationClasses evaluationClasses = new UserFunctionEvaluationClasses();


    public HashSet<String> variables = new HashSet<>();
    public ObjectVariablesArray variablesArray = new ObjectVariablesArray();
    public ArrayList<ArrayList<UserFunctionEvaluationClasses.ObjectResult>> valueList = new ArrayList<ArrayList<UserFunctionEvaluationClasses.ObjectResult>>();
    public ArrayList<Double> errorTable = new ArrayList<>();

    public static String approximationString = "100";

    public Button buttonCalculer;

    private boolean equationIsValid = false;
    private boolean resultIsValid = false;
    private boolean randomOrder = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_function);
        Intent receiveIntent = this.getIntent();


        buttonCalculer = (Button) findViewById(R.id.button_UserExpression_Calculer);

        final TextInputEditText editTextEquation = (TextInputEditText) findViewById(R.id.textInputEditText_UserExpression_Equation);
        final EditText editTextResult = (EditText) findViewById(R.id.textInputEditText_UserExpression_Result);


        /// <RECYCLERVIEW>

        final RecyclerView rvVariable = (RecyclerView)findViewById(R.id.recyclerViewVariable);

        final VariableAdapter adapter = new VariableAdapter(this,variablesArray);
        rvVariable.setLayoutManager(new LinearLayoutManager(this));
        rvVariable.setAdapter(adapter);
        rvVariable.setLayoutManager(new LinearLayoutManager(this));

        /// </RECYCLERVIEW>


        editTextEquation.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                evaluationClasses.setVariableArray(variablesArray,editTextEquation.getText().toString(), editTextResult.getText().toString(), variables);
                adapter.notifyDataSetChanged();
                adapter.notifyItemChanged(0);
            }

            @Override
            public void afterTextChanged(Editable editable) {
                evaluationClasses.setVariableArray(variablesArray,editTextEquation.getText().toString(),editTextResult.getText().toString(),variables);
                adapter.notifyDataSetChanged();
                adapter.notifyItemChanged(0);

                equationIsValid = checkExpression(editTextEquation.getText().toString(),variablesArray,editTextEquation);
                setButtonEnableFromText(buttonCalculer,variablesArray);
            }
        });


        editTextResult.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                evaluationClasses.setVariableArray(variablesArray,editTextEquation.getText().toString(), editTextResult.getText().toString(), variables);
                adapter.notifyDataSetChanged();
                adapter.notifyItemChanged(0);
            }

            @Override
            public void afterTextChanged(Editable editable) {
                evaluationClasses.setVariableArray(variablesArray,editTextEquation.getText().toString(),editTextResult.getText().toString(),variables);
                adapter.notifyDataSetChanged();
                adapter.notifyItemChanged(0);

                resultIsValid = checkExpression(editTextResult.getText().toString(),variablesArray,editTextResult);
                setButtonEnableFromText(buttonCalculer,variablesArray);
            }
        });



        buttonCalculer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                prepareExpressionEvaluation(editTextEquation.getText().toString(),
                        editTextResult.getText().toString(),
                        variablesArray,
                        errorTable);

                launchExpressionEvaluation(editTextEquation.getText().toString(),
                        editTextResult.getText().toString(),
                        variablesArray,
                        errorTable);
            }
        });

    }


    private boolean checkExpression(String expr, ObjectVariablesArray array, EditText editTextEquation)
    {
        double test;
        HashSet<String> vars = new HashSet<>();
        for (ObjectVariables var : array.Value)
        {
            vars.add(var.ID);
        }

        try {
            Expression exp = new ExpressionBuilder(expr).variables(vars).build();

            for (String var : vars)
            {
                exp.setVariable(var, Double.NaN);
            }
            test = exp.evaluate();
            editTextEquation.setError(null);

            return exp.validate().isValid();

        } catch (UnknownFunctionOrVariableException ex) {
            Log.e("VARIABLE", ex.getMessage());
            editTextEquation.setError(getString(R.string.text_activityUserFunction_EquationResultPrommpt_Error_Variable));
            return false;
        } catch(IllegalArgumentException ex) {
            Log.e("ARGUMENT",ex.getMessage());
            if (ex.getMessage().contains("Mismatched parentheses detected"))
            {
                editTextEquation.setError(getString(R.string.text_activityUserFunction_EquationResultPrommpt_Error_Parentheses));
            }
            else if (ex.getMessage().contains("Expression can not be empty"))
            {
                editTextEquation.setError(getString(R.string.text_activityUserFunction_EquationResultPrommpt_Error_Empty));
            }
            else if (ex.getMessage().contains("Operator is unknown for token"))
            {
                if (editTextEquation.getText().toString().contains("="))
                {
                    editTextEquation.setError(getString(R.string.text_activityUserFunction_EquationResultPrommpt_Error_EqualsOperator));
                }
                else
                {
                    editTextEquation.setError(getString(R.string.text_activityUserFunction_EquationResultPrommpt_Error_Operator));
                }
            }
            else if (ex.getMessage().contains("Invalid number of operands available"))
            {
                editTextEquation.setError(getString(R.string.text_activityUserFunction_EquationResultPrommpt_Error_Operator));
            }
            return false;
        }catch (Exception ex) {
            //Log.e("UNKNOWN", ex.getMessage());
            editTextEquation.setError(getString(R.string.text_activityUserFunction_EquationResultPrommpt_Error_Unknown));
            return false;
        }
    }

    private void SetApproximationString(EditText approximation)
    {
        double value;
        if (approximation.isEnabled())
        {
            if (approximation.getText().toString().matches(""))
            {
                approximation.setError(getText(R.string.text_activityUserFunction_ApproximationPrompt_Error_Empty));
                approximationString = null;
            }
            else
            {
                value = Double.parseDouble(approximation.getText().toString());
                if (value <= 0 || value > 100)
                {
                    approximation.setError(getText(R.string.text_activityUserFunction_ApproximationPrompt_Error_OutOfRange));
                    approximationString = null;
                }
                else
                {
                    approximation.setError(null);
                    approximationString = approximation.getText().toString();
                }
            }
        }
        else
        {
            approximationString = "100";
        }
    }

    private boolean checkApproximationString()
    {
        if (approximationString == null)
        {
            return false;
        }
        else
        {
            double approx;
            if (approximationString.isEmpty())
            {
                return false;
            }
            else
            {
                approx = Double.parseDouble(approximationString);
                return approx > 0.0 && approx <= 100.0;
            }
        }
    }

    public void setButtonEnable(boolean checkSettings)
    {
        boolean canCalculate = checkSettings && checkApproximationString() && equationIsValid && resultIsValid;
        buttonCalculer.setEnabled(canCalculate);
    }

    public void setButtonEnableFromText(Button button, ObjectVariablesArray array)
    {

        if (array.Value.isEmpty())
        {
            button.setEnabled(false);
            return;
        }
        for (ObjectVariables var : array.Value)
        {
            if(var.type == UserFunctionEvaluationClasses.ElectronicType.UNKNOWN)
            {
                if(var.listeValeurs.isEmpty())
                {
                button.setEnabled(false);
                return;
            }
                button.setEnabled(false);
                return;
            }
            
           if(checkApproximationString() == false)
           {
               button.setEnabled(false);
               return;
           }
        }
        if(equationIsValid == false || resultIsValid == false)
        {
            button.setEnabled(false);
            return;
        }

        button.setEnabled(true);
        return;
    }

    public void prepareExpressionEvaluation(String equation ,String result, ObjectVariablesArray array,ArrayList<Double> errorTable)
    {
        for (int i = 0;i<10;i++)
        {
            errorTable.add(Double.POSITIVE_INFINITY);
        }
        //evaluationClasses.findExpressionAnswer(variablesArray,equation,result,approximation,errorTable,valueList);
    }

    public void launchExpressionEvaluation(String equationString ,String resultString, ObjectVariablesArray objVarArray,ArrayList<Double> errorTable)
    {


        Intent intent = new Intent(this, ProcessEvaluations.class);
        intent.putExtra("UserFunctinon.objectVariableArray",variablesArray);
        intent.putExtra("UserFunctinon.equationString",equationString);
        intent.putExtra("UserFunctinon.resultString",resultString);
        intent.putExtra("UserFunctinon.errorTable",errorTable);

        startActivity(intent);
    }
}
