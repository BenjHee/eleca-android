package org.foss.greenapplication.eleca;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

public class PontDiviseur extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Intent receiveIntent = this.getIntent();


        CheckBox checkboxE3 = (CheckBox) findViewById(R.id.checkBoxE3);
        CheckBox checkboxE6 = (CheckBox) findViewById(R.id.checkBoxE6);
        CheckBox checkboxE12 = (CheckBox) findViewById(R.id.checkBoxE12);
        CheckBox checkboxE24 = (CheckBox) findViewById(R.id.checkBoxE24);
        CheckBox checkboxE48 = (CheckBox) findViewById(R.id.checkBoxE48);
        CheckBox checkboxE96 = (CheckBox) findViewById(R.id.checkBoxE96);

        final RadioButton radioEs = (RadioButton) findViewById (R.id.radioButton_ES);
        final RadioButton radioRatio = (RadioButton) findViewById (R.id.radioButtonRatio);
        final TextView textRatio = (TextView) findViewById(R.id.textViewRatio);
        final TextView textEntree = (TextView) findViewById(R.id.textViewESIn);
        final TextView textSortie = (TextView) findViewById(R.id.textViewEsOut);
        final EditText editTextRatio = (EditText) findViewById(R.id.editTextRatio);
        final EditText editTextEntree = (EditText) findViewById(R.id.editTextES_In);
        final EditText editTextSortie = (EditText) findViewById(R.id.editTextES_Out);


        boolean checkE3 = false,checkE6 = false,checkE12 = false,checkE24 = false,checkE48 = false,checkE96=false;

        final Button buttonCalculer = (Button) findViewById(R.id.buttonCalculer);
        buttonCalculer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                calculationSet(view);
            }
        });

        radioEs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                textEntree.setEnabled(true);
                editTextEntree.setEnabled(true);
                textSortie.setEnabled(true);
                editTextSortie.setEnabled(true);

                radioRatio.setChecked(false);
                textRatio.setEnabled(false);
                editTextRatio.setEnabled(false);
                buttonValidity();

            }
        });

        radioRatio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                textRatio.setEnabled(true);
                editTextRatio.setEnabled(true);

                radioEs.setChecked(false);
                textEntree.setEnabled(false);
                editTextEntree.setEnabled(false);
                textSortie.setEnabled(false);
                editTextSortie.setEnabled(false);
                buttonValidity();
            }
        });

        // END

        // calculation validation

        checkboxE3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                buttonValidity();
            }
        });
        checkboxE6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                buttonValidity();
            }
        });
        checkboxE6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                buttonValidity();
            }
        });
        checkboxE12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                buttonValidity();
            }
        });
        checkboxE24.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                buttonValidity();
            }
        });
        checkboxE24.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                buttonValidity();
            }
        });
        checkboxE48.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                buttonValidity();
            }
        });
        checkboxE96.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                buttonValidity();
            }
        });


        editTextEntree.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2)
            {
                buttonValidity();
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2)
            {
                buttonValidity();
            }

            @Override
            public void afterTextChanged(Editable editable)
            {
                buttonValidity();

            }
        });
        editTextSortie.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2)
            {
                buttonValidity();
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2)
            {
                buttonValidity();
            }

            @Override
            public void afterTextChanged(Editable editable)
            {
                buttonValidity();

            }
        });
        editTextRatio.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2)
            {
                buttonValidity();
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2)
            {
                buttonValidity();
            }

            @Override
            public void afterTextChanged(Editable editable)
            {
                buttonValidity();

            }
        });






        
    }

    private void buttonValidity()
    {
        Button buttonCalculer = (Button) findViewById(R.id.buttonCalculer);

        CheckBox checkboxE3 = (CheckBox) findViewById(R.id.checkBoxE3);
        CheckBox checkboxE6 = (CheckBox) findViewById(R.id.checkBoxE6);
        CheckBox checkboxE12 = (CheckBox) findViewById(R.id.checkBoxE12);
        CheckBox checkboxE24 = (CheckBox) findViewById(R.id.checkBoxE24);
        CheckBox checkboxE48 = (CheckBox) findViewById(R.id.checkBoxE48);
        CheckBox checkboxE96 = (CheckBox) findViewById(R.id.checkBoxE96);

        RadioButton radiobuttonEs = (RadioButton) findViewById(R.id.radioButton_ES);
        RadioButton radiobuttonRatio = (RadioButton) findViewById(R.id.radioButtonRatio);

        EditText edittextEntree = (EditText) findViewById(R.id.editTextES_In);
        EditText edittextSortie = (EditText) findViewById(R.id.editTextES_Out);
        EditText edittextRatio = (EditText) findViewById(R.id.editTextRatio);

        boolean paramTextIsValid = false;
        boolean checkboxSelected = false;



        // series selection
        if(checkboxE3.isChecked() == true) checkboxSelected = true;
        else if (checkboxE6.isChecked() == true) checkboxSelected = true;
        else if (checkboxE12.isChecked() == true) checkboxSelected = true;
        else if (checkboxE24.isChecked() == true) checkboxSelected = true;
        else if (checkboxE48.isChecked() == true) checkboxSelected = true;
        else if (checkboxE96.isChecked() == true) checkboxSelected = true;
        else checkboxSelected = false;


        String entreeText = edittextEntree.getText().toString().trim();
        String sortieText = edittextSortie.getText().toString().trim();
        String ratioText = edittextRatio.getText().toString().trim();



        if(radiobuttonEs.isChecked())
        {
            if(entreeText.isEmpty() == false && entreeText.length() != 0 && entreeText.equals("") == false
                    && sortieText.isEmpty() == false && sortieText.length() != 0 && sortieText.equals("") == false)
            {
                float entree = Float.valueOf(entreeText);
                float sortie = Float.valueOf(sortieText);
                if (entree > sortie && entree * sortie > 0)
                {
                    paramTextIsValid = true;
                }
            }
            else
                paramTextIsValid = false;
        }
        else if (radiobuttonRatio.isChecked())
        {
            if(ratioText.isEmpty() == false && ratioText.length() != 0 && ratioText.equals("") == false)
            {
                float ratio = Float.valueOf(ratioText);
                if (ratio > 0 && ratio < 1)
                {
                    paramTextIsValid = true;
                }
                else
                    paramTextIsValid = false;
            }
            else
                paramTextIsValid = false;
        }


        if (checkboxSelected == true && paramTextIsValid == true)
        {
            buttonCalculer.setEnabled(true);
        }
        else buttonCalculer.setEnabled(false);
    }

    private void calculationSet (View view)
    {
        CheckBox checkboxE3 = (CheckBox) findViewById(R.id.checkBoxE3);
        CheckBox checkboxE6 = (CheckBox) findViewById(R.id.checkBoxE6);
        CheckBox checkboxE12 = (CheckBox) findViewById(R.id.checkBoxE12);
        CheckBox checkboxE24 = (CheckBox) findViewById(R.id.checkBoxE24);
        CheckBox checkboxE48 = (CheckBox) findViewById(R.id.checkBoxE48);
        CheckBox checkboxE96 = (CheckBox) findViewById(R.id.checkBoxE96);

        RadioButton radiobuttonEs = (RadioButton) findViewById(R.id.radioButton_ES);
        RadioButton radiobuttonRatio = (RadioButton) findViewById(R.id.radioButtonRatio);

        EditText edittextEntree = (EditText) findViewById(R.id.editTextES_In);
        EditText edittextSortie = (EditText) findViewById(R.id.editTextES_Out);
        EditText edittextRatio = (EditText) findViewById(R.id.editTextRatio);


        boolean checkE3 = false,checkE6 = false,checkE12 = false,checkE24 = false,checkE48 = false,checkE96=false;
        float ratio = 0;
        double Vs = 0;

        if (checkboxE3.isChecked())
            checkE3 = true;
        else
            checkE3 = false;
        if (checkboxE6.isChecked())
            checkE6 = true;
        else
            checkE6 = false;
        if (checkboxE12.isChecked())
            checkE12 = true;
        else
            checkE12 = false;
        if (checkboxE24.isChecked())
            checkE24 = true;
        else
            checkE24 = false;
        if (checkboxE48.isChecked())
            checkE48 = true;
        else
            checkE48 = false;
        if (checkboxE96.isChecked())
            checkE96 = true;
        else
            checkE96 = false;


        if(radiobuttonEs.isChecked())
        {
            float entree = Float.valueOf(edittextEntree.getText().toString().trim());
            float sortie = Float.valueOf(edittextSortie.getText().toString().trim());
            ratio = sortie/entree;
            Vs = entree;

        }
        else if (radiobuttonRatio.isChecked())
        {
            float ratioIn = Float.valueOf(edittextRatio.getText().toString().trim());
            ratio = ratioIn;
            Vs = -1;
        }

        FonctionDeCalculs pontDiviseur = new FonctionDeCalculs();
        pontDiviseur.calculPontDiviseur(ratio,checkE3,checkE6,checkE12,checkE24,checkE48,checkE96);

        double rapportOut = pontDiviseur.getSolutionRapport();
        double valeurResistanceR1 = pontDiviseur.getR1();
        double valeurResistanceR2 = pontDiviseur.getR2();
        double solutionR1 = pontDiviseur.getSolutionR1();
        double solutionR2 = pontDiviseur.getSolutionR2();
        double ecartPuissance = pontDiviseur.getEcartPuissance();
        double puissanceR1 = pontDiviseur.getSolutionR1Puissance();
        double puissanceR2 = pontDiviseur.getSolutionR2Puissance();

        if(radiobuttonEs.isChecked())
        {
            Vs = Vs * rapportOut;
        }

        Intent intent = new Intent(this, ResultPontDiviseur.class);
        intent.putExtra("pontdiviseur.rapportCiblePontDiviseur",ratio);
        intent.putExtra("pontdiviseur.rapportPontDiviseur",rapportOut);
        intent.putExtra("pontdiviseur.valeurResistanceR1",valeurResistanceR1);
        intent.putExtra("pontdiviseur.valeurResistanceR2",valeurResistanceR2);
        intent.putExtra("pontdiviseur.solutionR1",solutionR1);
        intent.putExtra("pontdiviseur.solutionR2",solutionR2);
        intent.putExtra("pontdiviseur.puissanceR1",puissanceR1);
        intent.putExtra("pontdiviseur.puissanceR2",puissanceR2);
        intent.putExtra("pontdiviseur.ecartPuissance",ecartPuissance);
        intent.putExtra("calculationSet.Vs",Vs);
        startActivity(intent);

    }


}
