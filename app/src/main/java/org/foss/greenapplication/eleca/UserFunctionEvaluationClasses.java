package org.foss.greenapplication.eleca;

import android.os.Handler;
import android.os.Message;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;


/**
 * Created by infinite on 07/01/18.
 */

public class UserFunctionEvaluationClasses
{

    UserFunctionEvaluationClasses(Handler Handler)
    {
        this.functionHandler = Handler;
    }
    int countOfFinishedEvaluation = 0;
    int i = 0;
    UserFunctionEvaluationClasses ()
    {}

    Handler functionHandler = new Handler();

    int SerieE3[]  =   {100,220,470};
    int SerieE6[]  =   {100,150,220,330,470,680};
    int SerieE12[] =   {100,120,150,180,220,270,330,390,470,560,680,820};
    int SerieE24[] =   {100,110,120,130,150,160,180,200,220,240,270,300,330,360,390,430,470,510,560,620,680,750,820,910};
    int SerieE48[] =   {100,105,110,115,121,127,133,140,147,154,162,169,178,187,196,205,215,226,237,249,261,274,287,301,316,332,348,365,383,402,422,442,464,487,511,536,562,590,619,649,681,715,750,787,825,866,909,953};
    int SerieE96[] =   {100,102,105,107,110,113,115,118,121,124,127,130,133,137,140,143,147,150,154,158,162,165,169,174,178,182,187,191,196,200,205,210,215,221,226,232,237,243,249,255,261,267,274,280,287,294,301,309,316,324,332,340,348,357,365,374,383,392,402,412,422,432,442,453,464,475,487,499,511,523,536,549,562,576,590,604,619,634,649,665,681,698,715,732,750,768,787,806,825,845,866,887,909,931,953,976};
    int SerieE192[] = {100,101,102,104,105,106,107,109,110,111,113,114,115,117,118,120,121,123,124,126,127,129,130,132,133,135,137,138,140,142,143,145,147,149,150,152,154,156,158,160,162,164,165,167,169,172,174,176,178,180,182,184,187,189,191,193,196,198,200,203,205,208,210,213,215,218,221,223,226,229,232,234,237,240,243,246,249,252,255,258,261,264,267,271,274,277,280,284,287,291,294,298,301,305,309,312,316,320,324,328,332,336,340,344,348,352,357,361,365,370,374,379,383,388,392,397,402,407,412,417,422,427,432,437,442,448,453,459,464,470,475,481,487,493,499,505,511,517,523,530,536,542,549,556,562,569,576,583,590,597,604,612,619,626,634,642,649,657,665,673,681,690,698,706,715,723,732,741,750,759,768,777,787,796,806,816,825,835,845,856,866,876,887,898,909,920,931,942,953,965,976,988};

    enum ElectronicType
    {
        RESISTANCE,
        CONDENSATEUR,
        BOBINE,
        UNKNOWN
    }

    public static class ObjectVariables implements Serializable
    {
        ObjectVariables(String ID)
        {
            this.ID = ID;
            this.type = ElectronicType.UNKNOWN;
            this.listeValeurs = new HashSet<Double>();
        }

        Double getFirstValue()
        {
            for (Double val : listeValeurs)
            {
                return val;
            }
            return null;
        }
        String ID;
        ElectronicType type;
        HashSet<Double> listeValeurs;


    }

    public static class ObjectResult
    {
        ObjectResult(String ID, ElectronicType type,double value)
        {
            this.ID=ID;
            this.type = type;
            this.value = value;
        }
        void copy(ObjectResult res)
        {
            this.ID = res.ID;
            this.type = res.type;
            this.value = res.value;
        }
        String ID;
        ElectronicType type;
        double value;
    }

    public static class ResultSet
    {
        ResultSet(Double rslt, Double err)
        {
            result = rslt;
            error = err;
            value = new ArrayList<>();
        }
        ResultSet(ArrayList<ObjectResult> results,Double  rslt, Double err)
        {
            int i = 0;
            this.value.add(new ObjectResult(null,ElectronicType.UNKNOWN,Double.NaN));
            while (this.value.size() != results.size()) this.value.add(new ObjectResult(null,ElectronicType.UNKNOWN,Double.NaN));
            for(ObjectResult res : results)
            {
                res.copy(results.get(i));
                i++;
            }
            result = rslt;
            error = err;
        }
        void copyList (ArrayList<ObjectResult> results)
        {
            int i = 0;
            while (this.value.size() != results.size()) this.value.add(new ObjectResult(null,ElectronicType.UNKNOWN,Double.NaN));
            for(ObjectResult res : results)
            {
                res.copy(results.get(i));
                i++;
            }
        }
        void copy(ResultSet target)
        {
            int i = 0;
            this.result = target.result;
            this.error = target.error;
            for(ObjectResult res : this.value)
            {
                res.copy(target.value.get(i));
                i++;
            }
        }
        ArrayList<ObjectResult> value;
        Double result;
        Double error;

    }


    public static class ObjectVariablesArray implements Serializable
    {
        ArrayList<ObjectVariables> Value;


        //Constructor:
        public ObjectVariablesArray()
        {
            this.Value = new ArrayList<>();
        }

        //Usual Methods
        void add(ObjectVariables var)
        {
            this.Value.add(var);
        }
        void copy(ObjectVariablesArray objArray)
        {
            this.Value.removeAll(this.Value);
            for (ObjectVariables var : objArray.Value)
            {
                this.Value.add(var);
            }
        }
        void removeAll ()
        {
            this.Value.removeAll(this.Value);
        }
        ObjectVariables get(int position)
        {
            return this.Value.get(position);
        }
        ObjectVariables getByID(String ID)
        {
            for(ObjectVariables var : this.Value)
            {
                if (var.ID == ID) return var;
            }
            return null;
        }
        int size()
        {
            return this.Value.size();
        }


    }
    public static class ObjectValue
    {
        ObjectValue(String ID, Double value)
        {
            this.ID = ID;
            this.value = value;
            this.hash = ID.toString() + value.toString();
        }
        String ID;
        Double value;
        String hash;
    }
// ###############################################################



    static Map<String, ObjectVariables> dictionnaireDesVariables = new LinkedHashMap<>();


    public void setVariableArray (ObjectVariablesArray array, String equation, String result, HashSet<String> variableList)
    {
        variableList.clear();
        variableList.addAll(findExpressionVariables(equation));
        variableList.addAll(findExpressionVariables(result));
        array.removeAll();
        for (String var : variableList)
        {
            array.add(new ObjectVariables(var));
        }
    }

    public HashSet<String> findExpressionVariables(String Expression)
    {
        HashSet<String> MatchesSet = new HashSet<String>();
        Matcher matcher = Pattern.compile("\\b[a-zA-Z]{1}\\b")
                .matcher(Expression);
        while (matcher.find())
        {
            MatchesSet.add(matcher.group());
        }
        return MatchesSet;

    }

    public void fillVariableList (ObjectVariablesArray array,ObjectVariables target,boolean E3,boolean E6,boolean E12,boolean E24,boolean E48,boolean E96, boolean E192)
    {
        HashSet<Double> swap = new HashSet<>();
        swap.removeAll(swap);
        if(target.type == ElectronicType.RESISTANCE) {
            if (E3) for (int val : SerieE3)
                for (double i = 0.01; i <= 1e9; i = i * 10) swap.add(val * i);
            if (E6) for (int val : SerieE6)
                for (double i = 0.01; i <= 1e9; i = i * 10) swap.add(val * i);
            if (E12) for (int val : SerieE12)
                for (double i = 0.01; i <= 1e9; i = i * 10) swap.add(val * i);
            if (E24) for (int val : SerieE24)
                for (double i = 0.01; i <= 1e9; i = i * 10) swap.add(val * i);
            if (E48) for (int val : SerieE48)
                for (double i = 0.01; i <= 1e9; i = i * 10) swap.add(val * i);
            if (E96) for (int val : SerieE96)
                for (double i = 0.01; i <= 1e9; i = i * 10) swap.add(val * i);
            if (E192) for (int val : SerieE192)
                for (double i = 0.01; i <= 1e9; i = i * 10) swap.add(val * i);
        }
        else if (target.type == ElectronicType.CONDENSATEUR)
        {
            if(E3) for (int val : SerieE3)
                for (double i = 1e-14;i<=1e-3;i=i*10) swap.add(val*i);
            if (E6) for (int val : SerieE6)
                for (double i = 1e-14;i<=1e-3;i=i*10) swap.add(val*i);
            if (E12) for (int val : SerieE12)
                for (double i = 1e-14;i<=1e-3;i=i*10) swap.add(val*i);
            if (E24) for (int val : SerieE24)
                for (double i = 1e-14;i<=1e-3;i=i*10) swap.add(val*i);
            if (E48) for (int val : SerieE48)
                for (double i = 1e-14;i<=1e-3;i=i*10) swap.add(val*i);
            if (E96) for (int val : SerieE96)
                for (double i = 1e-14;i<=1e-3;i=i*10) swap.add(val*i);
            if (E192) for (int val : SerieE192)
                for (double i = 1e-14;i<=1e-3;i=i*10) swap.add(val*i);
        }
        else if (target.type == ElectronicType.BOBINE)
        {
            if(E3) for (int val : SerieE3)
                for (double i =1e-12;i<=1e-3;i=i*10) swap.add(val*i);
            if (E6) for (int val : SerieE6)
                for (double i = 1e-12;i<=1e-3;i=i*10) swap.add(val*i);
            if (E12) for (int val : SerieE12)
                for (double i = 1e-12;i<=1e-3;i=i*10) swap.add(val*i);
            if (E24) for (int val : SerieE24)
                for (double i = 1e-12;i<=1e-3;i=i*10) swap.add(val*i);
            if (E48) for (int val : SerieE48)
                for (double i = 1e-12;i<=1e-3;i=i*10) swap.add(val*i);
            if (E96) for (int val : SerieE96)
                for (double i = 1e-12;i<=1e-3;i=i*10) swap.add(val*i);
            if (E192) for (int val : SerieE192)
                for (double i = 1e-12;i<=1e-3;i=i*10) swap.add(val*i);
        }
        array.getByID(target.ID).listeValeurs.clear();
        array.getByID(target.ID).listeValeurs.addAll(swap);
    }


    public ArrayList<ObjectResult> setListOfValue (ObjectVariablesArray objVarArray)
    {
        ArrayList<ObjectVariables> arrayList = new ArrayList<>();
        for(ObjectVariables var : objVarArray.Value)
        {
            arrayList.add(var);
        }
        String fristVarID = arrayList.get(0).ID;
        int indexLastVar =arrayList.size() - 1;
        String lastVar = arrayList.get(indexLastVar).ID;
        ArrayList<ObjectResult> valueList = new ArrayList<>();
        valueList = fillListofValue(arrayList,fristVarID,lastVar,valueList);
        return valueList;
    }

    public ArrayList<ObjectResult> fillListofValue (ArrayList<ObjectVariables> arrayList, String firstVarID, String lastVar,ArrayList<ObjectResult> valueList)
    {

        for (ObjectVariables var: arrayList)
        {
            for (double val : var.listeValeurs )
            {
                valueList.add(new ObjectResult(var.ID, var.type, val));
                if (arrayList.size() > 1)
                    fillListofValue(new ArrayList<ObjectVariables>(arrayList.subList(1, arrayList.size())), firstVarID, lastVar, valueList);
            }
            if (var.ID == firstVarID) return valueList;
            else break;
        }
        return valueList;
    }

    public ArrayList<ResultSet> findExpressionAnswer(ObjectVariablesArray objVarArray,String equationString, String resultString,double resultApproximation,ArrayList<Double> errorTable, ArrayList<ResultSet> resultSets)
    {
        countOfFinishedEvaluation = 0;
        ArrayList<ObjectVariables> arrayList = new ArrayList<>();
        HashSet<String> varStringList = new HashSet<>();
        int numberOfEvaluation = 0;
        int i = 0,j=0;
        int numberOfEvaluationTemp = 0;

        // TODO: REMOVE VALUELIST
        /*while (valueList.size()!= 10)
        {
            valueList.add(new ArrayList<ObjectResult>());
        }*/

        while(resultSets.size() != 10)
        {
            resultSets.add(new ResultSet(Double.NaN,Double.POSITIVE_INFINITY));
        }

        ArrayList<ObjectResult> valueArrayBuffer = new ArrayList<>();

        for(ObjectVariables var : objVarArray.Value)
        {
            varStringList.add(var.ID);
            arrayList.add(var);
        }

        // count items
        numberOfEvaluation = 0;
        for(i = 0; i< arrayList.size();i++){
            numberOfEvaluationTemp = 1;
            for(j = 0; j< arrayList.size()-i;j++){
                numberOfEvaluationTemp *= arrayList.get(j).listeValeurs.size();
            }
            numberOfEvaluation += numberOfEvaluationTemp;
        }

        Message evaluationMessage_numberOfEvaluation = new Message(); // arg1 = 2
        evaluationMessage_numberOfEvaluation.arg1 = 2;
        evaluationMessage_numberOfEvaluation.arg2 = numberOfEvaluation;
        functionHandler.handleMessage(evaluationMessage_numberOfEvaluation);



        String firstVarID = arrayList.get(0).ID;
        int indexLastVar =arrayList.size() - 1;
        String lastVar = arrayList.get(indexLastVar).ID;

        Expression expEquation = new ExpressionBuilder(equationString).variables(varStringList).build();
        Expression expResult = new ExpressionBuilder(resultString).variables(varStringList).build();

        for(ObjectVariables var : objVarArray.Value)
        {
            expEquation.setVariable(var.ID,var.getFirstValue());
            expResult.setVariable(var.ID,var.getFirstValue());
            valueArrayBuffer.add(new ObjectResult(var.ID,var.type,var.getFirstValue()));
        }
        resultSets = testNewAnswer(arrayList,firstVarID,lastVar,resultSets,valueArrayBuffer,expEquation,expResult,resultApproximation,errorTable);

        Message evaluationMessage_finish = new Message(); // arg1 = 1
        evaluationMessage_finish.arg1 = 1;
        evaluationMessage_finish.obj = null;
        functionHandler.sendMessage(evaluationMessage_finish);
        numberOfEvaluation = 0;

        Message evaluationMessage_quickanswer = new Message();
        evaluationMessage_quickanswer.arg1 = 4;
        evaluationMessage_quickanswer.obj = resultSets;
        functionHandler.sendMessageAtFrontOfQueue(evaluationMessage_quickanswer);

        return resultSets;
    }



    public ArrayList<ResultSet> testNewAnswer (ArrayList<ObjectVariables> arrayList, String firstVarID, String lastVar,ArrayList<ResultSet> resultSets,ArrayList<ObjectResult> valueArrayBuffer,Expression expEquation,Expression expResult,double resultApproximation,ArrayList<Double> errorTable)
    {

        for (ObjectVariables var: arrayList)
        {
            for (double val : var.listeValeurs )
            {
                operation(expEquation,expResult,resultSets,valueArrayBuffer,resultApproximation,var,val,errorTable);
                if (arrayList.size() > 1)
                    testNewAnswer(new ArrayList<ObjectVariables>(arrayList.subList(1, arrayList.size())), firstVarID, lastVar, resultSets,valueArrayBuffer,expEquation,expResult,resultApproximation,errorTable);
            }
            if (var.ID == firstVarID) return resultSets;
            else break;
        }
        return resultSets;
    }

    public void operation(Expression expEquation, Expression expResult, ArrayList<ResultSet> resultSets,ArrayList<ObjectResult>valueArrayBuffer,double resultApproximation,ObjectVariables currentVar,double currentValue,ArrayList<Double> errorTable)
    {
        boolean evaluationIsComplete;
        double evaluationError;
        double equation = Double.NaN;
        double result = Double.NaN;
        expEquation.setVariable(currentVar.ID,currentValue);
        expResult.setVariable(currentVar.ID,currentValue);
        for(ObjectResult var : valueArrayBuffer)
        {
            if (var.ID == currentVar.ID)
            {
                var.value = currentValue;
            }
        }
        try
        {
            equation = expEquation.evaluate();
            result = expResult.evaluate();
            evaluationIsComplete = true;
        }catch (Exception ex)
        {
            evaluationIsComplete = false;
        }

        if (evaluationIsComplete)
        {
            evaluationError = Math.abs(equation - result);
            if (evaluationError < resultSets.get(9).error)
                handleResultSetsTables(resultSets,errorTable,valueArrayBuffer,evaluationError,equation);
        }
        Message evaluationMessage_count = new Message();
        countOfFinishedEvaluation ++;
        evaluationMessage_count.arg1 = 3;
        evaluationMessage_count.obj = countOfFinishedEvaluation;
        functionHandler.removeCallbacksAndMessages(null);
        functionHandler.sendMessage(evaluationMessage_count);
    }
    public void handleResultSetsTables(ArrayList<ResultSet> resultSets, ArrayList<Double> errorTable, ArrayList<ObjectResult>valueArrayBuffer, Double newerror,Double newResult)
    {
        int i=0;
        int j;
        Double errorSwap;
        Double errorSwapResultSet;
        Double newErrorResultSet = newerror;

        ArrayList<ObjectResult>valueArraySwap = new ArrayList<>();
        ArrayList<ObjectResult>valueArraySwapTwo = new ArrayList<>();

        Double resultSwap;
        Double resultSwapTwo;

        for (Double err : errorTable)
        {
            if (newerror<err)
            {break;}
            i++;
        }
        if (i < 10)
        {
            for (j = i;j<10;j++)
            {
                    errorSwap = errorTable.get(j);
                    errorTable.set(j, newerror);
                    newerror = errorSwap;

                    errorSwapResultSet = resultSets.get(j).error;
                    resultSets.get(j).error = newErrorResultSet;
                    newErrorResultSet = errorSwap;

                    resultSwap = resultSets.get(j).result;
                    resultSets.get(j).result = newResult;
                    newResult = resultSwap;
            }


            copyArrayListOfObjectResult(valueArraySwap,resultSets.get(i).value);
            copyArrayListOfObjectResult(resultSets.get(i).value,valueArrayBuffer);



            for(j=i+1;j<9;j++)
            {
                copyArrayListOfObjectResult(valueArraySwapTwo,resultSets.get(j).value);
                copyArrayListOfObjectResult(resultSets.get(j).value,valueArraySwap);
                copyArrayListOfObjectResult(valueArraySwap,valueArraySwapTwo);

            }
        }
    }
    public void copyArrayListOfObjectResult(ArrayList<ObjectResult> target, ArrayList<ObjectResult> source)
    {
        int index = 0;

        while (target.size() != source.size())
        {
            if(target.size() > source.size())
            {
                target.remove(0);
            }
            else if(target.size() < source.size())
            {
                target.add(new ObjectResult("",ElectronicType.UNKNOWN,Double.NaN));
            }
        }
        for (ObjectResult res : target)
        {
            res.copy(source.get(index));
            index++;

        }
    }

}
