package org.foss.greenapplication.eleca;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import org.foss.greenapplication.eleca.UserFunctionEvaluationClasses.ObjectVariablesArray;


import java.util.ArrayList;

import static android.R.attr.defaultValue;

public class ProcessEvaluations extends AppCompatActivity  {

    static Context context;

    @SuppressLint("HandlerLeak")
    Handler expressionThreadHandler = new Handler()
    {
        boolean numberOfEvaluationIsSet = false;
        @Override
        public void handleMessage(Message msg) {

            TextView textViewPending = (TextView) findViewById(R.id.textView_processEvaluation_pending);
            TextView textViewCount = (TextView) findViewById(R.id.textView_processEvaluation_count);

            if(msg.arg1 == 1)
            {
                textViewPending.setText("Done");
            }
            else if (msg.arg1 == 2)
            {
                nuberOfEvaluation = msg.arg2;

            }
            else if (msg.arg1 == 3)
            {
                evaluationCount = (int) msg.obj;
                textViewCount.setText(evaluationCount + " / " + nuberOfEvaluation);
                //this.removeCallbacksAndMessages(null);
            }
            else if (msg.arg1 == 4)
            {
                String quickAnswer = new String();
                for (UserFunctionEvaluationClasses.ObjectResult res : resultSets.get(0).value)
                {
                    switch (res.type)
                    {
                        case RESISTANCE:
                            quickAnswer += res.ID + " : " + formatResistanceString(res.value) + "\n";
                            break;
                        case CONDENSATEUR:
                            quickAnswer += res.ID + " : " + formatCondensateurString(res.value) + "\n";
                            break;
                        case BOBINE:
                            quickAnswer += res.ID + " : " + formatBobineString(res.value) + "\n";
                            break;
                        case UNKNOWN:
                            quickAnswer += res.ID + " : " + res.value + "\n";
                            break;
                    }
                }
            }
            else if (msg.arg1 == 5)
            {}
        }

    };

    UserFunctionEvaluationClasses evaluationClasses = new UserFunctionEvaluationClasses(expressionThreadHandler);



    public ArrayList<ArrayList<UserFunctionEvaluationClasses.ObjectResult>> valueList = new ArrayList<ArrayList<UserFunctionEvaluationClasses.ObjectResult>>();
    public ArrayList<UserFunctionEvaluationClasses.ResultSet> resultSets = new ArrayList<>();

    ObjectVariablesArray variablesArray;
    String equationString;
    String resultString;
    Double resultApproximation;
    ArrayList<Double> errorTable;
    int evaluationCount = 0;
    int nuberOfEvaluation = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        context = this;
        setContentView(R.layout.activity_process_evaluations);
        Intent receiveIntent = this.getIntent();

        variablesArray = (ObjectVariablesArray) receiveIntent.getSerializableExtra("UserFunctinon.objectVariableArray");
        equationString = receiveIntent.getStringExtra("UserFunctinon.equationString");
        resultString = receiveIntent.getStringExtra("UserFunctinon.resultString");
        resultApproximation = receiveIntent.getDoubleExtra("pontdiviseur.valeurResistanceR2", defaultValue);
        errorTable = (ArrayList<Double>) receiveIntent.getSerializableExtra("UserFunctinon.errorTable");

        final TextView textViewPending = (TextView) findViewById(R.id.textView_processEvaluation_pending);
        final TextView textViewCount = (TextView) findViewById(R.id.textView_processEvaluation_count);
        final TextView textViewEquation = (TextView) findViewById(R.id.textView_processEvaluation_equation);
        final TextView textViewResult = (TextView) findViewById(R.id.textView_processEvaluation_result);

        textViewEquation.setText(equationString);
        textViewResult.setText(resultString);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /// <RECYCLERVIEW>

        final RecyclerView rvSimpleResult = (RecyclerView)findViewById(R.id.recyclerViewResult);

        final ResultAdapter adapter = new ResultAdapter(this,resultSets);
        rvSimpleResult.setLayoutManager(new LinearLayoutManager(this));
        rvSimpleResult.setAdapter(adapter);
        rvSimpleResult.setLayoutManager(new LinearLayoutManager(this));

        /// </RECYCLERVIEW>

        final Runnable UIThread_updateRecyclerView = new Runnable() {
            @Override
            public void run() {
                adapter.notifyDataSetChanged();
                adapter.notifyItemChanged(0);
            }
        };
        Runnable evaluationRunnable = new Runnable() {
            @Override
            public void run() {
                evaluationClasses.findExpressionAnswer(variablesArray,equationString,resultString,resultApproximation,errorTable,resultSets);
                runOnUiThread(UIThread_updateRecyclerView);

            }
        };


        Thread evaluationThread = new Thread(evaluationRunnable);
        evaluationThread.start();
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        if(hasFocus)
        {
            Runnable UIRunnable = new Runnable() {
                @Override
                public void run() {

                }
            };
        }
    }








    // FORMATTAGE DES STRING DES VALEURS ///////////////////////////////////////////////////////////


    @SuppressLint("DefaultLocale")
    public String formatResistanceString(Double value)
    {
        if(value >= 1_000_000_000)
        {
            value = value /1_000_000_000;
            return (String.format("%.1f",value) + getResources().getString(R.string.text_common_E9ohm));
        }
        else if (value >= 1_000_000)
        {
            value = value /1_000_000;
            return (String.format("%.1f",value) + getResources().getString(R.string.text_common_E6ohm));
        }
        else if (value >= 1_000)
        {
            value = value /1_000;
            return (String.format("%.1f",value) + getResources().getString(R.string.text_common_E3ohm));
        }
        else
        {
            return (String.format("%.1f",value) + getResources().getString(R.string.text_common_E0ohm));
        }
    }

    @SuppressLint("DefaultLocale")
    public String formatCondensateurString(Double value)
    {
        if(value >= 1)
        {
            return (String.format("%.1f",value) + getResources().getString(R.string.text_common_E0F));
        }
        else if (value >= 1e-3)
        {
            value = value*1e3;
            return (String.format("%.1f",value) + getResources().getString(R.string.text_common_Em3F));
        }

        else if (value >= 1e-6)
        {
            value = value*1e6;
            return (String.format("%.1f",value) + getResources().getString(R.string.text_common_Em6F));
        }

        else if (value >= 1e-9)
        {
            value = value*1e9;
            return (String.format("%.1f",value) + getResources().getString(R.string.text_common_Em9F));
        }

        else if (value >= 1e-12)
        {
            value = value*1e12;
            return (String.format("%.1f",value) + getResources().getString(R.string.text_common_Em12F));
        }
        else
        {
            value = value*1e12;
            return (String.format("%.1f",value) + getResources().getString(R.string.text_common_Em12F));
        }
    }

    @SuppressLint("DefaultLocale")
    public String formatBobineString(Double value)
    {
        if(value >= 1)
        {
            return (String.format("%.1f",value) + getResources().getString(R.string.text_common_E0H));
        }
        else if (value >= 1e-3)
        {
            value = value*1e3;
            return (String.format("%.1f",value) + getResources().getString(R.string.text_common_Em3H));
        }

        else if (value >= 1e-6)
        {
            value = value*1e6;
            return (String.format("%.1f",value) + getResources().getString(R.string.text_common_Em6H));
        }

        else if (value >= 1e-9)
        {
            value = value*1e9;
            return (String.format("%.1f",value) + getResources().getString(R.string.text_common_Em9H));
        }

        else if (value >= 1e-12)
        {
            value = value*1e12;
            return (String.format("%.1f",value) + getResources().getString(R.string.text_common_Em12H));
        }
        else
        {
            value = value*1e12;
            return (String.format("%.1f",value) + getResources().getString(R.string.text_common_Em12H));
        }
    }
}


