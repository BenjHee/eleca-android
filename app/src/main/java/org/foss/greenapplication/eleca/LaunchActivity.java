package org.foss.greenapplication.eleca;

import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class LaunchActivity extends AppCompatActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)  {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch);
        Intent receiveIntent = this.getIntent();


        final ConstraintLayout constraintLayoutPontDiviseur = findViewById(R.id.constrainLayout_Home_PontDiviseur);
        constraintLayoutPontDiviseur.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                runPontDiviseur(view);
            }
        });
        final ConstraintLayout constraintLayoutUserEvaluation = findViewById(R.id.constrainLayout_Home_UserEvaluation);
        constraintLayoutUserEvaluation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                runUserExpression(view);
            }
        });

    }


    private void runUserExpression(View view)
    {
        Intent intent = new Intent(this, UserFunction.class);
        startActivity(intent);
    }
    private void runPontDiviseur(View view)
    {
        Intent intent = new Intent(this, PontDiviseur.class);
        startActivity(intent);
    }
}
