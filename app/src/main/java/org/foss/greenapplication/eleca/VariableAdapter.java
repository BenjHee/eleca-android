package org.foss.greenapplication.eleca;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.TextView;

import org.foss.greenapplication.eleca.UserFunctionEvaluationClasses.*;

/**
 * Created by infinite on 07/01/18.
 */

public class VariableAdapter extends RecyclerView.Adapter<VariableAdapter.ViewHolder>
{
    UserFunctionEvaluationClasses Evaluation = new UserFunctionEvaluationClasses();

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        public TextView variableNameTextView;
        public RadioButton variableRadioButtonResistance;
        public RadioButton variableRadioButtonCondensateur;
        public RadioButton variableRadioButtonbobine;

        public CheckBox variableItem_checkBoxE3;
        public CheckBox variableItem_checkBoxE6;
        public CheckBox variableItem_checkBoxE12;
        public CheckBox variableItem_checkBoxE24;
        public CheckBox variableItem_checkBoxE48;
        public CheckBox variableItem_checkBoxE96;
        public CheckBox variableItem_checkBoxE192;


        public ViewHolder(View itemView)
        {
            super(itemView);

            variableNameTextView = itemView.findViewById(R.id.item_textView_Varname);
            variableRadioButtonResistance = itemView.findViewById(R.id.item_radioButtonResistance);
            variableRadioButtonCondensateur = itemView.findViewById(R.id.item_radioButtonCondensateur);
            variableRadioButtonbobine = itemView.findViewById(R.id.item_radioButtonBobine);

            variableItem_checkBoxE3 = itemView.findViewById(R.id.item_checkBoxE3);
            variableItem_checkBoxE6 = itemView.findViewById(R.id.item_checkBoxE6);
            variableItem_checkBoxE12 = itemView.findViewById(R.id.item_checkBoxE12);
            variableItem_checkBoxE24 = itemView.findViewById(R.id.item_checkBoxE24);
            variableItem_checkBoxE48 = itemView.findViewById(R.id.item_checkBoxE48);
            variableItem_checkBoxE96 = itemView.findViewById(R.id.item_checkBoxE96);
            variableItem_checkBoxE192 = itemView.findViewById(R.id.item_checkBoxE192);


        }

    }
    // ####################################################################################

        private ObjectVariablesArray mVariable;
        private UserFunction mContext;

        public VariableAdapter(UserFunction context, ObjectVariablesArray variables)
        {
            mVariable = variables;
            mContext = context;
        }

        private Context getContext()
        {
            return mContext;
        }

    // ####################################################################################

    @Override
    public VariableAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View variableView = inflater.inflate(R.layout.item_variable, parent, false);
        ViewHolder viewHolder = new ViewHolder(variableView);

        inflater.inflate(R.layout.activity_user_function,parent,false);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder (final VariableAdapter.ViewHolder viewHolder, final int position)
    {
        final ObjectVariables variable = mVariable.get(position);

        TextView variable_textView = viewHolder.variableNameTextView;
        variable_textView.setText(variable.ID);
        final RadioButton resistance_radiobutton = viewHolder.variableRadioButtonResistance;
        final RadioButton condensateur_radiobutton = viewHolder.variableRadioButtonCondensateur;
        final RadioButton bobine_radiobutton = viewHolder.variableRadioButtonbobine;

        final CheckBox serieE3_checkbox = viewHolder.variableItem_checkBoxE3;
        final CheckBox serieE6_checkbox = viewHolder.variableItem_checkBoxE6;
        final CheckBox serieE12_checkbox = viewHolder.variableItem_checkBoxE12;
        final CheckBox serieE24_checkbox = viewHolder.variableItem_checkBoxE24;
        final CheckBox serieE48_checkbox = viewHolder.variableItem_checkBoxE48;
        final CheckBox serieE96_checkbox = viewHolder.variableItem_checkBoxE96;
        final CheckBox serieE192_checkbox = viewHolder.variableItem_checkBoxE192;

        resistance_radiobutton.setChecked(false);
        condensateur_radiobutton.setChecked(false);
        bobine_radiobutton.setChecked(false);

        serieE3_checkbox.setChecked(false);
        serieE6_checkbox.setChecked(false);
        serieE12_checkbox.setChecked(false);
        serieE24_checkbox.setChecked(false);
        serieE48_checkbox.setChecked(false);
        serieE96_checkbox.setChecked(false);
        serieE192_checkbox.setChecked(false);



        resistance_radiobutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                condensateur_radiobutton.setChecked(false);
                bobine_radiobutton.setChecked(false);
                mVariable.get(position).type = ElectronicType.RESISTANCE;
                Evaluation.fillVariableList(mVariable,mVariable.get(position),
                        serieE3_checkbox.isChecked(),
                        serieE6_checkbox.isChecked(),
                        serieE12_checkbox.isChecked(),
                        serieE24_checkbox.isChecked(),
                        serieE48_checkbox.isChecked(),
                        serieE96_checkbox.isChecked(),
                        serieE192_checkbox.isChecked());
                getContext();
                mContext.setButtonEnable(checkSettings(mVariable));

            }
        });
        condensateur_radiobutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resistance_radiobutton.setChecked(false);
                bobine_radiobutton.setChecked(false);
                mVariable.get(position).type = ElectronicType.CONDENSATEUR;
                Evaluation.fillVariableList(mVariable,mVariable.get(position),
                        serieE3_checkbox.isChecked(),
                        serieE6_checkbox.isChecked(),
                        serieE12_checkbox.isChecked(),
                        serieE24_checkbox.isChecked(),
                        serieE48_checkbox.isChecked(),
                        serieE96_checkbox.isChecked(),
                        serieE192_checkbox.isChecked());
                mContext.setButtonEnable(checkSettings(mVariable));


            }
        });
        bobine_radiobutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resistance_radiobutton.setChecked(false);
                condensateur_radiobutton.setChecked(false);
                mVariable.get(position).type = ElectronicType.BOBINE;
                Evaluation.fillVariableList(mVariable,mVariable.get(position),
                        serieE3_checkbox.isChecked(),
                        serieE6_checkbox.isChecked(),
                        serieE12_checkbox.isChecked(),
                        serieE24_checkbox.isChecked(),
                        serieE48_checkbox.isChecked(),
                        serieE96_checkbox.isChecked(),
                        serieE192_checkbox.isChecked());
                mContext.setButtonEnable(checkSettings(mVariable));
            }
        });



        serieE3_checkbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Evaluation.fillVariableList(mVariable,mVariable.get(position),
                        serieE3_checkbox.isChecked(),
                        serieE6_checkbox.isChecked(),
                        serieE12_checkbox.isChecked(),
                        serieE24_checkbox.isChecked(),
                        serieE48_checkbox.isChecked(),
                        serieE96_checkbox.isChecked(),
                        serieE192_checkbox.isChecked());
                mContext.setButtonEnable(checkSettings(mVariable));
            }
        });
        serieE6_checkbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Evaluation.fillVariableList(mVariable,mVariable.get(position),
                        serieE3_checkbox.isChecked(),
                        serieE6_checkbox.isChecked(),
                        serieE12_checkbox.isChecked(),
                        serieE24_checkbox.isChecked(),
                        serieE48_checkbox.isChecked(),
                        serieE96_checkbox.isChecked(),
                        serieE192_checkbox.isChecked());
                  mContext.setButtonEnable(checkSettings(mVariable));
            }
        });
        serieE12_checkbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Evaluation.fillVariableList(mVariable,mVariable.get(position),
                        serieE3_checkbox.isChecked(),
                        serieE6_checkbox.isChecked(),
                        serieE12_checkbox.isChecked(),
                        serieE24_checkbox.isChecked(),
                        serieE48_checkbox.isChecked(),
                        serieE96_checkbox.isChecked(),
                        serieE192_checkbox.isChecked());
                getContext();
                mContext.setButtonEnable(checkSettings(mVariable));
            }
        });
        serieE24_checkbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Evaluation.fillVariableList(mVariable,mVariable.get(position),
                        serieE3_checkbox.isChecked(),
                        serieE6_checkbox.isChecked(),
                        serieE12_checkbox.isChecked(),
                        serieE24_checkbox.isChecked(),
                        serieE48_checkbox.isChecked(),
                        serieE96_checkbox.isChecked(),
                        serieE192_checkbox.isChecked());
                mContext.setButtonEnable(checkSettings(mVariable));
            }
        });
        serieE48_checkbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Evaluation.fillVariableList(mVariable,mVariable.get(position),
                        serieE3_checkbox.isChecked(),
                        serieE6_checkbox.isChecked(),
                        serieE12_checkbox.isChecked(),
                        serieE24_checkbox.isChecked(),
                        serieE48_checkbox.isChecked(),
                        serieE96_checkbox.isChecked(),
                        serieE192_checkbox.isChecked());
                mContext.setButtonEnable(checkSettings(mVariable));
            }
        });
        serieE96_checkbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Evaluation.fillVariableList(mVariable,mVariable.get(position),
                        serieE3_checkbox.isChecked(),
                        serieE6_checkbox.isChecked(),
                        serieE12_checkbox.isChecked(),
                        serieE24_checkbox.isChecked(),
                        serieE48_checkbox.isChecked(),
                        serieE96_checkbox.isChecked(),
                        serieE192_checkbox.isChecked());
                mContext.setButtonEnable(checkSettings(mVariable));
            }
        });
        serieE192_checkbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Evaluation.fillVariableList(mVariable,mVariable.get(position),
                        serieE3_checkbox.isChecked(),
                        serieE6_checkbox.isChecked(),
                        serieE12_checkbox.isChecked(),
                        serieE24_checkbox.isChecked(),
                        serieE48_checkbox.isChecked(),
                        serieE96_checkbox.isChecked(),
                        serieE192_checkbox.isChecked());
                mContext.setButtonEnable(checkSettings(mVariable));
            }
        });
    }

    @Override
    public int getItemCount()
    {
        return mVariable.size();
    }


    public boolean checkSettings(ObjectVariablesArray array)
    {
        for(ObjectVariables variable: array.Value)
        {
            if(variable.listeValeurs.isEmpty())
            {
                return false;
            }
            if(variable.type == ElectronicType.UNKNOWN)
            {
                return false;
            }
        }
        return true;
    }


    public void refillVariableList()
    {
        if (mVariable.size() != 0)
        {
            for (ObjectVariables var : mVariable.Value)
            {
                Evaluation.fillVariableList(mVariable,mVariable.getByID(var.ID),
                        ((CheckBox)((Activity)getContext()).findViewById(R.id.item_checkBoxE3)).isChecked(),
                        ((CheckBox)((Activity)getContext()).findViewById(R.id.item_checkBoxE6)).isChecked(),
                        ((CheckBox)((Activity)getContext()).findViewById(R.id.item_checkBoxE12)).isChecked(),
                        ((CheckBox)((Activity)getContext()).findViewById(R.id.item_checkBoxE24)).isChecked(),
                        ((CheckBox)((Activity)getContext()).findViewById(R.id.item_checkBoxE48)).isChecked(),
                        ((CheckBox)((Activity)getContext()).findViewById(R.id.item_checkBoxE96)).isChecked(),
                        ((CheckBox)((Activity)getContext()).findViewById(R.id.item_checkBoxE192)).isChecked());
                mContext.setButtonEnable(checkSettings(mVariable));
            }
        }
    }


}
