package org.foss.greenapplication.eleca;

import android.support.v7.app.AppCompatActivity;


public class TraitementDesValuers extends AppCompatActivity{

    public String formatResistanceString(Double value)
    {
        if(value >= 1_000_000_000)
        {
            value =value /1_000_000_000;
            return (String.format("%.1f",value) + getResources().getString(R.string.text_common_E9ohm));
        }
        else if (value >= 1_000_000)
        {
            value =value /1_000_000;
            return (String.format("%.1f",value) + getResources().getString(R.string.text_common_E6ohm));
        }
        else if (value >= 1_000)
        {
            value =value /1_000;
            return (String.format("%.1f",value) + getResources().getString(R.string.text_common_E3ohm));
        }
        else
        {
            return (String.format("%.1f",value) + getResources().getString(R.string.text_common_E0ohm));
        }
    }

    public String formatCondensateurString(Double value)
    {
        if (value >= 1e-3)
        {
            value = value*100;
            return (String.format("%.1f",value) + getResources().getString(R.string.text_common_Em3F));
        }

        else if (value >= 1e-6)
        {
            value = value*100_000;
            return (String.format("%.1f",value) + getResources().getString(R.string.text_common_Em6F));
        }

        else if (value >= 1e-9)
        {
            value = value*100_000;
            return (String.format("%.1f",value) + getResources().getString(R.string.text_common_Em9F));
        }

        else if (value >= 1e-12)
        {
            value = value*100_000;
            return (String.format("%.1f",value) + getResources().getString(R.string.text_common_Em12F));
        }
        else
        {
            return (String.format("%.1f",value) + getResources().getString(R.string.text_common_E0F));
        }
    }

    public String formatBobineString(Double value)
    {
        if (value >= 1e-3)
        {
            value = value*100;
            return (String.format("%.1f",value) + getResources().getString(R.string.text_common_Em3H));
        }

        else if (value >= 1e-6)
        {
            value = value*100_000;
            return (String.format("%.1f",value) + getResources().getString(R.string.text_common_Em6H));
        }

        else if (value >= 1e-9)
        {
            value = value*100_000;
            return (String.format("%.1f",value) + getResources().getString(R.string.text_common_Em9H));
        }

        else if (value >= 1e-12)
        {
            value = value*100_000;
            return (String.format("%.1f",value) + getResources().getString(R.string.text_common_Em12H));
        }
        else
        {
            return (String.format("%.1f",value) + getResources().getString(R.string.text_common_E0H));
        }
    }


}
