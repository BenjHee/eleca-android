package org.foss.greenapplication.eleca;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import org.foss.greenapplication.eleca.UserFunctionEvaluationClasses.*;

import java.util.ArrayList;

public class ResultAdapter extends RecyclerView.Adapter<ResultAdapter.ViewHolder> {

    ProcessEvaluations processEvaluation = new ProcessEvaluations();
    public class ViewHolder extends RecyclerView.ViewHolder
    {
        public TextView valueListNumber;
        public TextView valuesResult;
        public TextView valuesError;
        public TextView valuesNames;
        public TextView valuesTypes;
        public TextView valuesValue;

        public ViewHolder(View itemView)
        {
            super(itemView);
            valueListNumber = itemView.findViewById(R.id.itemSimpleResult_numberTitle);
            valuesResult = itemView.findViewById(R.id.itemSimpleResult_result);
            valuesError = itemView.findViewById(R.id.itemSimpleResult_error);

            valuesNames = itemView.findViewById(R.id.itemSimpleResult_variables);
            valuesTypes = itemView.findViewById(R.id.itemSimpleResult_types);
            valuesValue = itemView.findViewById(R.id.itemSimpleResult_values);
        }
    }

    // #############################################################################################

    private ArrayList<ResultSet> mVariable;
    private ProcessEvaluations mContext;

    public ResultAdapter(ProcessEvaluations context, ArrayList<ResultSet> variables)
    {
        mVariable = variables;
        mContext = context;
    }

    private Context getContext()
    {
        return mContext;
    }
    // #############################################################################################

    @Override
    public ResultAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View resultView = inflater.inflate(R.layout.item_result,parent,false);
        ViewHolder viewHolder = new ViewHolder(resultView);

        inflater.inflate(R.layout.activity_process_evaluations,parent,false);
        return viewHolder;
    }

    @SuppressLint("DefaultLocale")
    @Override
    public void onBindViewHolder (final ResultAdapter.ViewHolder viewHolder, final int position)
    {
        final ArrayList<ObjectResult> variable = mVariable.get(position).value;
        TextView variablesListNumber_textView = viewHolder.valueListNumber;
        TextView variablesResult_textView = viewHolder.valuesResult;
        TextView variablesError_textView = viewHolder.valuesError;
        TextView variablesNames_textView = viewHolder.valuesNames;
        TextView variablesTypes_textView = viewHolder.valuesTypes;
        TextView variablesValues_textView = viewHolder.valuesValue;



        variablesListNumber_textView.setText(R.string.text_activityProcessEvaluations_RecyclerView_ValueListTtle);
        variablesListNumber_textView.append(String.format("%d",position+1));

        variablesResult_textView.setText(R.string.text_activityProcessEvaluations_RecyclerView_Result);
        variablesResult_textView.append(String.format("%f",mVariable.get(position).result));
        variablesError_textView.setText(R.string.text_activityProcessEvaluations_RecyclerView_Error);
        variablesError_textView.append(String.format("%f",mVariable.get(position).error));

        variablesNames_textView.setText("");
        variablesTypes_textView.setText("");
        variablesValues_textView.setText("");
        for(ObjectResult res : variable)
        {
            variablesNames_textView.append(res.ID + "\n");

            variablesTypes_textView.append(res.type + "\n");

            switch (res.type)
            {
                case RESISTANCE:
                    variablesValues_textView.append(mContext.formatResistanceString(res.value) + "\n");
                    break;
                case CONDENSATEUR:
                    variablesValues_textView.append(mContext.formatCondensateurString(res.value) + "\n");
                    break;
                case BOBINE:
                    variablesValues_textView.append(mContext.formatBobineString(res.value) + "\n");
                    break;
            }
        }
    }

    @Override
    public int getItemCount()
    {
        return mVariable.size();
    }
}
